
// Команда на отдельную папку node_modules
// ln -s /Users/front-end_developer/node_modules
// ln -s /Users/front-end_dev/work_wrap/node_modules
// После нее уже запуск gulp

// Установка нескольких пакетов
// npm i --save-dev gulp-sass browser-sync gulp-concat gulp-uglifyjs gulp-cssnano gulp-rename del gulp-autoprefixer gulp-twig


// Установить базу
// /Applications/MAMP/Library/bin/mysql -u root -p shopware_xcite_db < shopware_xcite_db.sql
// пароль root
// test@example.com / shopware авторизация




// Gulp 3
if (gulpVersion == 3) {
	// Html beautify
	gulp.task('htmlbeautify', function() {
		gulp.src( _project_src + '/*.html')
		.pipe(htmlbeautify({
			"indent_size": 4,
			"indent_with_tabs": true,
		}))
		.pipe(gulp.dest( _project_src + ''))
		.pipe(browserSync.reload({stream: true}))
	});



	gulp.task('watch', ['browser-sync', 'sass'], function() {
		gulp.watch( _project_src + '/_twig/**/*.twig', ['twig-compile']);
		gulp.watch( _project_src + '/web/css/**/*.sass', ['sass']);
		//gulp.watch( _project_src + '/*.html', ['htmlbeautify']);

		gulp.watch( _project_src + '/web/img/**/*.svg', browserSync.reload);
		gulp.watch( _project_src + '/web/js/**/*.js', browserSync.reload);
		gulp.watch( _project_src + '/**/*.html', browserSync.reload);
		gulp.watch( _project_src + '/**/*.php', browserSync.reload);
	});

	gulp.task('gulp-setup', function() {
		return gulp.src([
			'gulpfile.js', 
			'package.json',
			])
		.pipe(gulp.dest( _project_src + '/__gulp-setup'));
	});

	// Сохраняем настройки проекта
	gulp.task('default', ['watch', 'gulp-setup', 'twig-compile']);
}








// Gulp 4
if (gulpVersion == 4) {
	// Оптимизация изображеий
	gulp.task('del', () => {
		return del([ _project_src + '/web/img-compress/*'])
	});
	gulp.task('img-compress', () => {
		return gulp.src( _project_src + '/web/img/**')
		.pipe(imagemin({
			progressive: true
		}))
		.pipe(gulp.dest( _project_src + '/web/img-compress/'))
	});
	gulp.task('img-min', gulp.series('del', gulp.parallel('img-compress')));
	// Оптимизация изображеий END


	// Минификация 
	// gulp.task('scripts', function() {
	// 	return gulp.src([ // Берем все необходимые библиотеки
	// 	_project_src + '/frontend/web/libs/bowser-master/src/bowser.js', // Берем Плагин
	// 	_project_src + '/frontend/web/libs/bootstrap/js/bootstrap.min.js', // Берем Плагин
	// 	_project_src + '/frontend/web/libs/jquery.validate/jquery.validate.min.js', // Берем Плагин
	// 	_project_src + '/frontend/web/libs/cusel-min-2.5/cusel-min-2.5.js', // Берем Плагин
	// 	_project_src + '/frontend/web/libs/owl-carousel.2.4/owl.carousel.min.js', // Берем Плагин
	// 	_project_src + '/frontend/web/libs/magnific-popup/dist/jquery.magnific-popup.min.js', // Берем Плагин
	// 	_project_src + '/frontend/web/libs/fancyBox/source/jquery.fancybox.pack.js', // Берем Плагин
	// 	_project_src + '/frontend/web/libs/fancyBox/source/helpers/jquery.fancybox-media.js', // Берем Плагин
	// 	_project_src + '/frontend/web/libs/reel-v1.3-0/jquery.reel-min.js', // Берем Плагин
	// 	_project_src + '/frontend/web/libs/ion.rangeSlider/js/ion.rangeSlider.min.js', // Берем Плагин
	// 	])
	// 	.pipe(concat('libs.min.js')) // Собираем их в кучу в новом файле libs.min.js
	// 	.pipe(uglify()) // Сжимаем JS файл
	// 	.pipe(gulp.dest( _project_src + '/frontend/web/js')); // Выгружаем в папку app/js
	// });

	gulp.task('script_app', function() {
		return gulp.src( _project_src + '/web/js/app.js') // Берем источник
		.pipe(concat('app.min.js')) // Собираем их в кучу в новом файле libs.min.js
		.pipe(uglify()) // Сжимаем JS файл
		.pipe(gulp.dest( _project_src + '/web/js')) // Выгружаем в папку app/js
		.pipe(browserSync.reload({stream: true})) // Обновляем CSS на странице при изменении
	});


	gulp.task('htmlmin', () => {
		return gulp.src( _project_src + '/*.html')
		.pipe(htmlmin({ 
			collapseWhitespace: true,
			minifyCSS: true,
			minifyJS: true,
			removeComments: true,
		}))
		.pipe(gulp.dest( _project_src + '/html-min'));
	});
	// Минификация END

}




// var browserSync = require('browser-sync').create();
// gulp.task('browser-sync', function() {
// 	browserSync.init({
// 	//proxy: _project_url, // Директория для сервера (Proxy)
// 	server: {
// 	  baseDir: _project_src // Директория для сервера
// 	},
// 	open: "external",
// 	notify: false,
// });
// });