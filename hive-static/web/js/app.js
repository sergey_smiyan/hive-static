$(document).ready(function () {
	function is_touch_enabled() {
		return ( 'ontouchstart' in window ) ||
		( navigator.maxTouchPoints > 0 ) ||
		( navigator.msMaxTouchPoints > 0 );
	}

	if( is_touch_enabled() ) {
		$("html").addClass("touch");
	}
	else {
		$("html").addClass("no-touch");
	}

	// Definition of browser
	if (bowser.mac){$("html").addClass("mac-os");}
	if (bowser.windows){$("html").addClass("win-os");}
	if (bowser.safari){$("html").addClass("safari");}
	if (bowser.chrome){$("html").addClass("chrome");}
	if (bowser.msie){$("html").addClass("int-expl");}
	if (bowser.gecko){$("html").addClass("mozz");}
	if (/Edge/.test(navigator.userAgent)) {$("html").addClass("edge");}
	// Definition of browser END

	$(".preloader").delay(50).fadeOut();
	setTimeout(function(){
		$("body").addClass("page-loaded");
	}, 150);


	// Bootstrap Modal
	$('.modal').modal({
		show: false
	});


	// Button Up, Help Button
	$(".btn-top").click(function() {
		$("html, body").animate({ scrollTop: 0 }, 1000);
		return false;
	});

	$(window).scroll(function(){
		if ( $(document).scrollTop() > 200 ) {
			$('.btn-top').fadeIn('fast');
			$('.btn-top').addClass('on');
			$('.main-head, .inner-head').addClass('fixed');

		} else {
			$('.btn-top').fadeOut('fast');
			$('.btn-top').removeClass('on');
			$('.main-head, .inner-head').removeClass('fixed');
		}
	});
	// Button Up END


	// Client slider Slick
	$(".client-slider-wrap").each(function(){
		let sliderContainer = $(this);
		let clientSlider = $(this).find('.client-slider');
		let statusCurrent = sliderContainer.find('.paging-info .current');
		let statusTotal = sliderContainer.find('.paging-info .total');

		clientSlider.on('init reInit afterChange', function(slick, currentSlide){
			let i = (currentSlide ? currentSlide.currentSlide : 0) + 1;
			if (i < 10) {
				statusCurrent.text('0' + i);
			} else {
				statusCurrent.text(i);
			}

			if (currentSlide.slideCount < 10) {
				statusTotal.text('0' + currentSlide.slideCount);
			} else {
				statusTotal.text(currentSlide.slideCount);
			}
		});

		if ($(this).find('.client-slider .client-slider-itm').length > 1) {
			setTimeout(function(){
				clientSlider.slick({
					slidesToShow: 1,
					slideToScroll: 1,
					speed: 1000,
					swipe: false,
					swipeToSlide: false,
					touchThreshold: 10,
					autoplaySpeed: 5000,
					dots: false,
					appendArrows: sliderContainer.find(".slider-nav"),
					prevArrow: '<button class="slick-prev slide-arrow"></button>',
					nextArrow: '<button class="slick-next slide-arrow"></button>',
					autoplay: true,
					// adaptiveHeight: true,
					responsive: [
					{
						breakpoint: 992,
						settings: {
							speed: 1500,
							autoplaySpeed: 5000,
							fade: false,
							swipe: true,
						}
					}
					]
				});

				$('.slick-arrow').on('click',function() {
					clientSlider.slick('slickPause');
				});

			}, 100);
		}
	});

	// Features slider Slick
	$(".features-slider-wrap").each(function(){
		let sliderContainer = $(this);
		let featuresSlider = $(this).find('.features-slider');
		let statusCurrent = sliderContainer.find('.paging-info .current');
		let statusTotal = sliderContainer.find('.paging-info .total');

		featuresSlider.on('init reInit afterChange', function(slick, currentSlide){
			let i = (currentSlide ? currentSlide.currentSlide : 0) + 1;
			if (i < 10) {
				statusCurrent.text('0' + i);
			} else {
				statusCurrent.text(i);
			}

			if (currentSlide.slideCount < 10) {
				statusTotal.text('0' + currentSlide.slideCount);
			} else {
				statusTotal.text(currentSlide.slideCount);
			}
		});

		if ($(this).find('.features-slider .features-slider-itm').length > 1) {
			setTimeout(function(){
				featuresSlider.slick({
					slidesToShow: 1,
					slideToScroll: 1,
					speed: 1000,
					swipe: false,
					swipeToSlide: false,
					touchThreshold: 10,
					// autoplaySpeed: 5000,
					dots: false,
					appendArrows: sliderContainer.find(".slider-nav"),
					prevArrow: '<button class="slick-prev slide-arrow"></button>',
					nextArrow: '<button class="slick-next slide-arrow"></button>',
					autoplay: false,
					adaptiveHeight: true,
					fade: false,
					swipe: true,
				});

			}, 100);
		}
	});

	// NEED
	// Line-clamp p
	$('.more-btn').on('click',function(e) {
		e.preventDefault();

		let container = $('.uber-descr'),
			text = $(container).find('.text-hidden');

		if (text.hasClass('line-clamp')) {
			text.addClass('no-line-clamp');
			text.animate({"max-height": "1000px"}, 600, 'easeInCirc', () => {
				text.removeClass('line-clamp');
				$(this).addClass('open');
				$(this).text('Weniger lesen');
			});
		} else {
			text.animate({"max-height": "90px"}, 600, 'easeOutCirc', () => {
				setLineClamp(text);
				$(this).removeClass('open');
				$(this).text('Mehr lesen');
			});
		}
	});

	function setLineClamp(item) {
		item.removeClass('no-line-clamp')
		item.addClass('line-clamp');
	}


	// Scroll to the block .div by clicking
	$(".main-mnu").on("click","a[href*=#]", function (event) {
		event.preventDefault();
		let id  = $(this).attr('href'),
			top = $(id).offset().top - 100;
		$('body,html').animate({scrollTop: top}, 1000);
	});
	// Scroll to the block .div by clicking END

	// Custom scrollbar (Если включить .wrapper не работает параллакс)
	setTimeout(function(){
		$(".width-limit").mCustomScrollbar({
			axis:"x",
			theme:"dark",
			scrollButtons:{enable:false}
		});

	}, 25);
	// Custom scrollbar



	if ($(window).width() >= 992) {

		$.getScript("web/libs/parallax-div/parallax-1.js", function(){});

	}








});
// Document ready END
