// Parallax
$(".dashboard-page > .container").each(function () {

	// Двигающиеся блоки
	var ids = ['bg-figure-1', 'bg-figure-2'];

	var controller = new ScrollMagic.Controller({
		refreshInterval: 500,
		globalSceneOptions: {
			logLevel: 0
		}
	});

	var elPos = {
		getDocY: function() {
			return window.scrollY || document.documentElement.scrollTop;
		},

		getTop: function (el) {
			return el.getBoundingClientRect().top + this.getDocY() - document.documentElement.offsetTop;
		},

		getBottom: function (el) {
			return el.getBoundingClientRect().bottom + this.getDocY() - document.documentElement.offsetTop;
		}
	};

	let Parallax = function(el, options) {
		let _this = this;
		this.target = el;

		this.options = {
			speed: options.speed || _this.getSpeed(_this.target) || 1,
			scrollEl: options.scrollEl || _this.target.parentNode,
			offset: options.offset || 0,
			defaultTween: {
				translateY: function() {
					return _this.duration * (_this.speed / 10)
				},
				offset: 0
			}
		};

		this.speed = this.options.speed;
		this.scrollEl = this.options.scrollEl;
		this.duration = this.getDuration(_this.target);
		this.animOptions = this.getAnimOptions(_this.target);
		this.tween = Object.assign(
			_this.options.defaultTween, _this.animOptions
			);

		console.log(this.tween);

		this.timeline = anime.timeline({
			targets: _this.target,
			autoplay: false,
			loop: false,
			duration: 100,
			easing: 'linear',
			offset: 0
		});

		this.timeline.add(_this.tween)
		.add(_this.animOptions);

		this.addScene(this);

		return this;
	}

	Parallax.prototype.getDuration = function(target) {
		let duration = target.getAttribute('data-parallax-duration');
		if(duration !== null){
			return Number(duration);
		}
		return elPos.getBottom(target);
	};

	Parallax.prototype.getAnimOptions = function(target) {
		let opts = target.getAttribute('data-parallax-animation');
		if (opts !== null) {
			return JSON.parse(opts);
		}
		return {};
	};

	Parallax.prototype.getSpeed = function(target) {
		let speed = target.getAttribute('data-parallax-speed');
		if (speed !== null) {
			return Number(speed);
		}
		return 1;
	};

	Parallax.prototype.addScene = function(_this){
		let sceneDuration = _this.duration + (_this.duration * (_this.speed / 10));                                 
		_this.scene = new ScrollMagic.Scene({
			triggerHook: "onLeave",
			triggerElement: _this.scrollEl,
			duration: (_this.duration / 10) + sceneDuration
		});
		_this.scene.on('progress', function(event) {
			_this.timeline.seek(event.progress * 100);
		})
		//.addIndicators()
		.addTo(controller);
	};

	// Запуск скрипта от начала блока
	function init() {
		var scene = document.getElementById('sect-top-about-start');

		window.animations = ids.map(function(cID){
			let el = document.getElementById(cID);
			if (el !== null) {
				return new Parallax(el, {scrollEl: scene});
			}
		});

	}


	if (document.readyState !== 'loading') init();
	else document.addEventListener('DOMContentLoaded', init);

});
// Parallax END