Подключать:
<script src="web/libs/parallax-div/anime.js/lib/anime.min.js" defer></script>
<script src="web/libs/parallax-div/ScrollMagic.js" defer></script>
<script src="web/libs/parallax-div/debug.addIndicators.js" defer></script>
Или:
<script src="web/libs/parallax-div/anime.js/lib/anime.min.js" defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/ScrollMagic.js" defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.3/plugins/debug.addIndicators.js" defer></script>

В файл app.js добавить строчку:
$.getScript("web/libs/parallax-div/parallax-1.js", function(){});