var projectType = '1'; // Project type:  1-Static or 2-Proxy

var _project_src = 'hive-static';  // определяем директорию проекта и название
var _project_url = 'hive.loc';  // определяем домен для proxy



var gulpVersion  = '4'; // Gulp version: 3 or 4
var gulp         = require('gulp');
var sass         = require('gulp-sass');
var browserSync  = require('browser-sync');
var concat       = require('gulp-concat');
var uglify       = require('gulp-uglifyjs');
var cssnano      = require('gulp-cssnano');
var rename       = require('gulp-rename');
var del          = require('del');
var imagemin     = require('gulp-imagemin');
var autoprefixer = require('gulp-autoprefixer');
var twig         = require('gulp-twig');
var htmlbeautify = require('gulp-html-beautify');
var htmlmin_1    = require('gulp-html-minifier');
var htmlmin      = require('gulp-htmlmin');




// Project type:  1-Static ================================================================
if (projectType == 1) {
	var browserSync = require('browser-sync').create();
	gulp.task('browser-sync', function() {
		browserSync.init({
			server: {
			  baseDir: _project_src // Директория для сервера
			},
			open: "external",
			notify: false,
		});
	});


	// Compile Sass to CSS
	gulp.task('sass', function(){
		return gulp.src( _project_src + '/web/css/_sass/**/*.sass')
		.pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError)) // Преобразуем Sass в CSS (compressed - Сжимает, expanded - Не сжимает)
		.pipe(autoprefixer(['last 95 versions', '> 1%', 'ie 8', 'ie 7'], { cascade: true }))
		.pipe(gulp.dest( _project_src + '/web/css'))
		.pipe(browserSync.reload({stream: true}))
	});

	// Compile Twig templates to HTML
	gulp.task('twig-compile', function() {
		return gulp.src( _project_src + '/_twig/*.twig')
		.pipe(twig())
		.pipe(gulp.dest( _project_src + ''))
		.pipe(browserSync.reload({stream: true}))
	});

	// Html beautify
	gulp.task('htmlbeautify', () => {
		return gulp.src( _project_src + '/*.html')
		.pipe(htmlbeautify({
			"indent_size": 4,
			"indent_with_tabs": true,
		}))
		.pipe(gulp.dest( _project_src + ''))
	});
	gulp.task('twig', gulp.series('twig-compile', gulp.parallel('htmlbeautify')));



	gulp.task('watch', function() {
		//gulp.watch( _project_src + '/*.html', gulp.parallel('htmlbeautify'));
		gulp.watch( _project_src + '/_twig/**/*.twig', gulp.parallel('twig'));
		gulp.watch( _project_src + '/web/css/**/*.sass', gulp.parallel('sass'));
		gulp.watch( _project_src + '/web/css/**/*.scss', gulp.parallel('sass'));
		gulp.watch( _project_src + '/web/img/**/*.svg').on('change', browserSync.reload);
		gulp.watch( _project_src + '/web/js/**/*.js').on('change', browserSync.reload);
		gulp.watch( _project_src + '/**/*.html').on('change', browserSync.reload);
		gulp.watch( _project_src + '/**/*.php').on('change', browserSync.reload);
	});

	gulp.task('gulp-setup', function() {
		return gulp.src([
			'gulpfile.js',
			'package.json',
			'package-lock.json',
			'gulpfile-doc.js',
			])
		.pipe(gulp.dest( _project_src + '/__gulp-setup/_gulp_4'));
	});

	// Save projects settings
	gulp.task('default', gulp.parallel('watch', 'browser-sync', 'gulp-setup', 'twig'));


} /* projectType END */






// Project type:  2-Proxy ================================================================
if (projectType == 2) {
	var browserSync = require('browser-sync').create();
	gulp.task('browser-sync', function() {
		browserSync.init({
			proxy: _project_url,
			open: "external",
			notify: false,
		});
	});


	// Compile Sass to CSS
	gulp.task('sass', function(){
		return gulp.src( _project_src + '/css/_sass/**/*.sass')
		.pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError)) // Преобразуем Sass в CSS (compressed - Сжимает, expanded - Не сжимает)
		.pipe(autoprefixer(['last 95 versions', '> 1%', 'ie 8', 'ie 7'], { cascade: true }))
		.pipe(gulp.dest( _project_src + '/css'))
		.pipe(browserSync.reload({stream: true}))
	});


	gulp.task('watch', function() {
		gulp.watch( _project_src + '/css/**/*.sass', gulp.parallel('sass'));
		gulp.watch( _project_src + '/css/**/*.scss', gulp.parallel('sass'));
		gulp.watch( _project_src + '/img/**/*.svg').on('change', browserSync.reload);
		gulp.watch( _project_src + '/js/**/*.js').on('change', browserSync.reload);
		gulp.watch( _project_src + '/**/*.html').on('change', browserSync.reload);
		gulp.watch( _project_src + '/**/*.php').on('change', browserSync.reload);
	});

	gulp.task('gulp-setup', function() {
		return gulp.src([
			'gulpfile.js',
			'package.json',
			'package-lock.json',
			'gulpfile-doc.js',
			])
		.pipe(gulp.dest( _project_src + '/__gulp-setup/_gulp_4'));
	});

	// Save projects settings
	gulp.task('default', gulp.parallel('watch', 'browser-sync', 'gulp-setup'));


} /* projectType END */





